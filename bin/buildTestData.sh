#!/bin/bash

names=(
"Arya Stark"
"Bran Stark"
"Brienne of Tarth"
"Bronn"
"Catelyn Stark"
"Cersei Lannister"
"Daenerys Targaryen"
"Davos Seaworth"
"Eddard Stark"
"Gendry"
"Gilly"
"Jaime Lannister"
"Jeor Mormont"
"Joffrey Baratheon"
"Jon Snow"
"Jorah Mormont"
"Khal Drogo"
"Margaery Tyrell"
"Melisandre"
"Petyr Baelish"
"Ramsay Snow"
"Robb Stark"
"Robert Baratheon"
"Samwell Tarly"
"Sandor Clegane"
"Sansa Stark"
"Shae"
"Stannis Baratheon"
"Talisa Stark"
"Theon Greyjoy"
"Tormund Giantsbane"
"Tyrion Lannister"
"Tywin Lannister"
"Varys"
"Viserys Targaryen"
"Ygritte"
)

mkdir testDataKeys &>/dev/null
mkdir testDataCerts &>/dev/null

#THIS NEEDS TO BE THE DOMAIN NAME OF THE SERVER
SERVERNAME="localhost"

#Generate root authority private key
openssl genrsa -out rootCA.key 2048

#Generate and sign root authority certificate
openssl req -x509 -new -nodes -key rootCA.key -days 1024 -out rootCA.crt -subj "/C=AU/ST=private/L=WesternAustralia/O=Perth/CN=ROOTAUTHORITY"

#Generate Server Key
openssl genrsa -out server.key 2048

#Generate signing request from server key
openssl req -new -key server.key -out server.csr -subj "/C=AU/ST=private/L=WesternAustralia/O=Perth/CN=$SERVERNAME"

#Create a new certificate from the signing request signed with the root authority
openssl x509 -req -in server.csr -CA rootCA.crt -CAkey rootCA.key -CAcreateserial -out server.crt -days 1024

rm server.csr rootCA.srl

./trustcloud &
jobID=$!
function certDigest()
{
    openssl x509 -in testDataCerts/$1.crt -fingerprint -md5 -noout | \
        grep -oE "[^=]+$" | \
        sed -E "s/://g;y/$(echo {A..Z})/$(echo {a..z})/"
}

for i in "${names[@]}"; do
    tmpName=$(echo $i | sed "s/ /_/g");   
    openssl req -x509 -new -nodes -days 1024 -keyout testDataKeys/${tmpName}.key -out testDataCerts/${tmpName}.crt -subj "/C=AU/ST=private/L=Westeros/O=Kings Landing/CN=$i"

    ./trustcloud -u testDataCerts/${tmpName}.crt -h localhost
done;

for i in {1..75}; do
    name1=$(echo ${names[$(($RANDOM % ${#names[@]}))]} | sed "s/ /_/g")
    name2=$(echo ${names[$(($RANDOM % ${#names[@]}))]} | sed "s/ /_/g")

    echo "$name2 vouches for $name1"

    ./trustcloud -h localhost -V $(certDigest $name1) $(certDigest $name2) testDataKeys/$name2.key
done
sleep 2
kill $jobID
