/*
	CITS3002 Project 2014
	Name:				Dylan R. Johnston
	Student Number:		20770146
	Date: 				15/05/2014
*/
#include "trustcloud.h"

/*
 *	@name 	certificateDigest
 *	@param	fileData* X509* cert
 * 	@info	Returns a digest for a x509 certifitcate
 *	@return int error code
 */
char* certificateDigest(X509* cert)
{
	unsigned char md5[EVP_MAX_MD_SIZE];
	char* digest = memAlloc(NULL, EVP_MAX_MD_SIZE / 2 + 1);
	unsigned int mdLength;
	
	OpenSSL_add_all_digests();
	const EVP_MD* md = EVP_get_digestbyname("md5");
	
	X509_digest(cert, md, md5, &mdLength);
	for (unsigned int i = 0; i < mdLength; i++) sprintf(digest + 2*i, "%02x", md5[i]);
	return digest;
}

/*
 *	@name 	messageDigest
 *	@param	fileData* X509* cert
 * 	@info	Returns a digest for an arbitrary buffer.
 *	@return int error code
 */
char* messageDigest(void* buf, int size)
{
	unsigned char md5[EVP_MAX_MD_SIZE];
 	char* digest = memAlloc(NULL, EVP_MAX_MD_SIZE / 2 + 1);
 	unsigned int mdLength;

 	OpenSSL_add_all_digests();
 	const EVP_MD* md = EVP_get_digestbyname("md5");
	EVP_MD_CTX* mdctx = EVP_MD_CTX_create();

	EVP_DigestInit_ex(mdctx, md, NULL);
	EVP_DigestUpdate(mdctx, buf, size);
	EVP_DigestFinal_ex(mdctx, md5, &mdLength);
	EVP_MD_CTX_destroy(mdctx);

	for (unsigned int i = 0; i < mdLength; i++) sprintf(digest + 2*i, "%02x", md5[i]);
	return digest;
}

/*
 *	@name 	verifyDigest
 *	@param	void* buf, int size, char* digest
 * 	@info	Verifies a given digest for a given buffer.
 *	@return int 1 if the digest is correct, 0 otherwise
 */
int verifyDigest(void* buf, int size, char* digest)
{
	char* tmpDigest = messageDigest(buf, size);
	int result = strcmp(tmpDigest, digest);
	free(tmpDigest);
	return result ? 0 : 1; //strcmp is 0 for match, but we want to return 0 if no match so we can "if(verifyDigest(...))"
}
