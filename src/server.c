/*
	CITS3002 Project 2014
	Name:				Dylan R. Johnston
	Student Number:		20770146
	Date: 				15/05/2014
*/
#include "trustcloud.h"

/*
 *	@name 	serveConnectionInstance
 *	@param	SSL* ssl
 * 	@info	Serves a given connection instace, should be threadable, but ran out of time to implement + difficult to test.
 *	@return
 */
void serveConnectionInstance(SSL* ssl)
{	
	if (SSL_accept(ssl) == -1)
	{
		ERR_print_errors_fp(stderr);
	}
	else
	{
		Operations operation;																			//Just like on the client side, first thing is the operations ENUM
		SERVER_STATE serverState;
		chunkedSSL_read(ssl, &operation, sizeof(Operations));

		switch(operation)
		{
			case ADD:																					//Switch block for Adding a file
			{
				FileData* fileData = SSL_readFileData(ssl);
				serverState = writeOutUserFile(fileData) ? SERVER_STATE_SUCCESS : SERVER_STATE_FAILURE; //Capture the write success and report back to the client via the SERVER_STATE enum
				chunkedSSL_write(ssl, &serverState, sizeof(SERVER_STATE));
				FILEDATA_destoryFileData(fileData);
			}
				break;

			case FETCH:																					//Swtich block for fetching a file
			{
				int minTrustLength;
				chunkedSSL_read(ssl, &minTrustLength, sizeof(int));										//First thing after operations enum for fetching a file is the trust length
				
				char* digest = memAlloc(NULL, EVP_MAX_MD_SIZE / 2 + 1);									//We don't need to send/recieve the size of digest as it's always the same
				chunkedSSL_read(ssl, digest, EVP_MAX_MD_SIZE / 2);

				FileData* fileData = fileDataFromDigest(digest);										//Get the FileData struct and therefore data corresponding to the digest

				if (fileData != NULL)																	//Check that we did actually find a file with matching digest
				{
					if (!verifyDigest(fileData->data, fileData->size, digest))							//Verify that the found file still has the recorded digest
					{
						serverState = SERVER_STATE_FILE_TAMPERED;
						chunkedSSL_write(ssl, &serverState, sizeof(SERVER_STATE));
						free(digest);
						FILEDATA_destoryFileData(fileData);
						break;
					}

					UserTable* vouchers = fileVouchersFromFileData(fileData);							//List of users who have vouched for this file

					for (int i = 0; i < vouchers->count; i++)
					{
						int node = getUserIndexFromDigest(vouchers->users[i]->certificateDigest);		//Get the users index for the trust matrix from their certificate digest
																										//Then we check if there exists a ring of at least the required length in the trust matrix
						if (doesExistLargeEnoughRingForNode(globalTrustMatrix, node, minTrustLength) != -1) 
						{																				//That contains the user that's vouched for our file
							serverState = SERVER_STATE_FILE_EXISTS_AND_TRUSTED;
							chunkedSSL_write(ssl, &serverState, sizeof(SERVER_STATE));

							fileData->fileName = strrchr(fileData->fileName, '/') + 1;					//The fileName returned from fileDataFromDigest contains internal storage information
							SSL_writeFileData(ssl, fileData);											//The return of the strrchr is a pointer that points only to the actual file name not path
							break;
						}
					}

					serverState = SERVER_STATE_FILE_NOT_TRUSTED;										//We only reach here if we couldn't find a voucher or a well enough trusted one for this file
					chunkedSSL_write(ssl, &serverState, sizeof(SERVER_STATE));

				}
				else
				{
					serverState = SERVER_STATE_FILE_NOT_FOUND;											//The file digest sent from the client doesn't match any files we have stored
					chunkedSSL_write(ssl, &serverState, sizeof(SERVER_STATE));
					free(digest);
					break;
				}
			}
				break;

			case LIST:
			/***************************************************\
 			*  N O T     I M P L E M E N T E D     Y E T    :(	*
 			\***************************************************/
				break;

			case VOUCHFILE:																				//Swtich block for fetching a file
			{
				char* targetDigest = memAlloc(NULL, EVP_MAX_MD_SIZE / 2 + 1);							//The target digest here is the digest of the file we're vouching for
				chunkedSSL_read(ssl, targetDigest, EVP_MAX_MD_SIZE / 2);

				char* sourceDigest = memAlloc(NULL, EVP_MAX_MD_SIZE / 2 + 1);							//The source digest being the User vouching for file
				chunkedSSL_read(ssl, sourceDigest, EVP_MAX_MD_SIZE / 2);

				unsigned int size;																		//The signature I think is always 128 bytes, but I can't find any documented evidence so we'll assume it changes
				chunkedSSL_read(ssl, &size, sizeof(int));

				unsigned char* signature = memAlloc(NULL, size);

				chunkedSSL_read(ssl, signature, size);

				SERVER_STATE serverState;
				FileData* fileData;
				if ((fileData = fileDataFromDigest(targetDigest)) != NULL)								//Check if the file we're vouching for exists
				{
					serverState = checkVouch(targetDigest, sourceDigest, signature, size);				//Check the validity of the signature and its source
				}
				else
				{
					serverState = SERVER_STATE_VOUCH_TARGET_NOT_FOUND;
				}
				chunkedSSL_write(ssl, &serverState, sizeof(SERVER_STATE));								//Report back to the client the good/bad news

				if (serverState == SERVER_STATE_SUCCESS)
				{
					free(fileData->data);

					fileData->data = (char*)signature;													//Just highjacking the old fileData struct, mainly for the fileData->fileName
					fileData->size = (int)size;															//1 USER_FILES_DIRECTORY, 2 Digests, 2 '/'s, a ".sig", and a nullbyte walk into a bar.
					fileData->fileName = memAlloc(fileData->fileName, strlen(USER_FILES_DIRECTORY) + 2*EVP_MAX_MD_SIZE + 7);
					*strrchr(fileData->fileName, '/') = '\0';											//The following lines are dirty exploitations of strrchr, strcat, and pointers.
					strcat(fileData->fileName, "/");													//Bassically we take the last / of fileName and set it to the null byte making it the new end of the string
					strcat(fileData->fileName, sourceDigest);											//Then strcat can start copying in at the now null byted /
					strcat(fileData->fileName, ".sig");													//And we allocated enough memory above to fit all this.
					writeData(fileData);																//We now have the output path for the signature file.
				}
				
				if (fileData) FILEDATA_destoryFileData(fileData);										//fileData doesn't exist here if fileDataFromDigest fails
				free(targetDigest);
				free(sourceDigest);
			}
				break;

			case VOUCHUSER:																				//Swtich block for vouching for a user
			{
				char* targetDigest = memAlloc(NULL, EVP_MAX_MD_SIZE / 2 + 1);							//The target digest here is the digest of the certificate of the user we're vouching for
				chunkedSSL_read(ssl, targetDigest, EVP_MAX_MD_SIZE / 2);

				char* sourceDigest = memAlloc(NULL, EVP_MAX_MD_SIZE / 2 + 1);							//While sourceDigest is like above, the user doing the vouching
				chunkedSSL_read(ssl, sourceDigest, EVP_MAX_MD_SIZE / 2);

				unsigned int size;
				chunkedSSL_read(ssl, &size, sizeof(int));

				unsigned char* signature = memAlloc(NULL, size);

				chunkedSSL_read(ssl, signature, size);

				SERVER_STATE serverState;
				if (getUserFromDigest(targetDigest))													//Check to see if the target user exists in globalUserTable
				{
					serverState = checkVouch(targetDigest, sourceDigest, signature, size);				//If they do, check the validity of the supplied signature
				}
				else
				{
					serverState = SERVER_STATE_VOUCH_TARGET_NOT_FOUND;
				}
				chunkedSSL_write(ssl, &serverState, sizeof(SERVER_STATE));								//Tell the client the good/bad news

				if (serverState == SERVER_STATE_SUCCESS)												//If we're successfull we need to create a new file represnting the vouch
				{
					FileData* fileData = FILEDATA_createFileData();
					fileData->data = (char*)signature;
					fileData->size = (int)size;															//Most of this is the exact same as above only one directory deeper
					fileData->fileName = memAlloc(NULL, strlen(USER_TRUST_DIRECTORY) + EVP_MAX_MD_SIZE + 3);
					strcat(fileData->fileName, USER_TRUST_DIRECTORY);
					createDirectory(fileData->fileName);
					strcat(fileData->fileName, "/");
					strcat(fileData->fileName, targetDigest);
					createDirectory(fileData->fileName);
					strcat(fileData->fileName, "/");
					strcat(fileData->fileName, sourceDigest);
					writeData(fileData);
																										//Update the global trust matrix with our new but of trust
					globalTrustMatrix->data[getUserIndexFromDigest(sourceDigest)][getUserIndexFromDigest(targetDigest)] = 1;
					FILEDATA_destoryFileData(fileData);
				}
				
				free(targetDigest);
				free(sourceDigest);
			}	
				break;

			case UPLOAD:																				//The updload switch block, not as exciting as the others
			{
				FileData* fileData = SSL_readFileData(ssl);
				serverState = writeOutCertificate(fileData) ? SERVER_STATE_SUCCESS : SERVER_STATE_FAILURE;
				chunkedSSL_write(ssl, &serverState, sizeof(SERVER_STATE));
				
				FILEDATA_destoryFileData(fileData);
			}
				break;

			default:
				break;
		}
	}
	int socketDescriptor = SSL_get_fd(ssl);																//Finally close the sockets and free the structs
	SSL_free(ssl);
	close(socketDescriptor);
}

/*
 *	@name 	startServer
 *	@param	
 * 	@info	The non threadable part of the server model, runs once and listens for new connections, forking off new threads.
 *	@return
 */
void startServer()
{
	char* certificateFile = SERVER_CERTIFICATE;
	char* keyFile = SERVER_PRIVATE_KEY;

	SSL_library_init();

	SSL_CTX* ctx = initialiseServerCTX();
	
	loadServerCertificates(ctx, certificateFile, keyFile);												//Server certificate for hostname checking by the client

	loadUsers();																						//Load existing users from disk
	loadGlobalTrustMatrix();																			//Load existing trust relations from disk

	int socketDescriptor = openNewTCPListener(flags.port);

	while (1)
	{
		sockaddr_in addr;
		socklen_t len = sizeof(addr);

		int clientSocketDescriptor = accept(socketDescriptor, (sockaddr *)&addr, &len);

		SSL* ssl = SSL_new(ctx);
		SSL_set_fd(ssl, clientSocketDescriptor);

		serveConnectionInstance(ssl);
	}
	close(socketDescriptor);
	SSL_CTX_free(ctx);
}
