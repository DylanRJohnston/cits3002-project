/*
	CITS3002 Project 2014
	Name:				Dylan R. Johnston
	Student Number:		20770146
	Date: 				15/05/2014
*/
#include "trustcloud.h"

Flags flags = {NULL, NULL, NULL, NULL, 55989, 0, SERVER};	//Default global flags state
Matrix* globalTrustMatrix;
UserTable globalUserTable = {NULL, 0};

/*
 *	@name 	parseAndValidateFlags
 *	@param	int* argcPointer, char*** argvPointer
 * 	@info	Parses and validates the input flags
 *	@return
 */
 /*******************************************
 *	Doesn't do the validating part just yet
 *  Tends to segfault if paramaters are wrong
 ********************************************/
void parseAndValidateFlags(int* argcPointer, char*** argvPointer)
{
	int argc = *argcPointer;
	char** argv = *argvPointer;

	//Can't use getopt because of the -v flag, getopt doesn't support 2 parameter flags. :(
	for (int i = 1; i < argc; i++)
	{
		if (argv[i][0] != '-') continue;

		switch (argv[i][1])
		{
			case ADD:
				flags.operation = ADD;
				flags.fileName = argv[++i];
				break;

			case 'c':
				flags.requiredTrustCircumference = atoi(argv[++i]);
				break;

			case FETCH:
				flags.operation = FETCH;
				flags.fileName = argv[++i];
				break;

			case 'h':
				i++; //Increment i because it currently points to "-h"

				flags.hostName = argv[i];
				char* breakPoint = strchr(argv[i], ':'); //char* to the deliminator ':'
				if (breakPoint != NULL) //If the deliminator was supplied (Otherwise we have a default port)
				{
					flags.port = atoi(breakPoint + 1); //This is the part of the string past the ':'
					*breakPoint = '\0'; //Sets the deliminator to the null byte so references to flags.hostName will end here excluding the port number
				}
				break;

			case LIST:
				flags.operation = LIST;
				break;

			case UPLOAD:
				flags.operation = UPLOAD;
				flags.fileName = argv[++i];
				break;

			case VOUCHFILE:
				flags.operation = VOUCHFILE;
				flags.targetDigest = argv[++i];
				flags.sourceDigest = argv[++i];
				flags.fileName = argv[++i];
				break;

			case VOUCHUSER:
				flags.operation = VOUCHUSER;
				flags.targetDigest = argv[++i];
				flags.sourceDigest = argv[++i];
				flags.fileName = argv[++i];
				break;

			case 'p':
				flags.port = atoi(argv[++i]);
				break;
		}
	}
}

/*
 *	@name 	main
 *	@param	int argc, char** argv
 * 	@info	Main function, takes the command line input arguments and does their corresponding operation with them.
 *	@return
 */
int main(int argc, char** argv)
{
	parseAndValidateFlags(&argc, &argv);

	switch (flags.operation)
	{
		case ADD:
			uploadFileToServer();
			break;

		case FETCH:
			fetchFileFromServer();
			break;

		case LIST:
			getListFromServer();
			break;

		case VOUCHFILE:
			vouchForFile();
			break;

		case VOUCHUSER:
			vouchForUser();
			break;

		case UPLOAD:
			uploadCertificate();
			break;

		case SERVER:
			startServer();
			break;
	}
}
