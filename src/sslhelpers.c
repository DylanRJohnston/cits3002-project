/*
	CITS3002 Project 2014
	Name:				Dylan R. Johnston
	Student Number:		20770146
	Date: 				15/05/2014
*/
#include "trustcloud.h"

/*
 *	@name 	loadServerCertificates
 *	@param	SSL_CTX* ctx, char* certificateFile, char* keyFile
 * 	@info	Loads the server certificate in to the SSL context and checks it.
 *	@return
 */
void loadServerCertificates(SSL_CTX* ctx, char* certificateFile, char* keyFile)
{
	//Load the certificate into the SSL Context
	if (SSL_CTX_use_certificate_file(ctx, certificateFile, SSL_FILETYPE_PEM) <= 0)
	{
		ERR_print_errors_fp(stderr);
		exit(EXIT_FAILURE);
	}

	//Load the private into the SSL Context
	if (SSL_CTX_use_PrivateKey_file(ctx, keyFile, SSL_FILETYPE_PEM) <= 0)
	{
		ERR_print_errors_fp(stderr);
		exit(EXIT_FAILURE);
	}

	//Verify certificate and private key
	if (! SSL_CTX_check_private_key(ctx))
	{
		fprintf(stderr, "The given private key and certificate do no match\nCertFile:%s\nKeyFile%s\n", certificateFile, keyFile);
		exit(EXIT_FAILURE);
	}
}

/*
 *	@name 	openNewTCPConnection
 *	@param	char* hostname, int port
 * 	@info	Opens a new tcp connection to hostname on port (port), returns the socket descriptor of the connection.
 *	@return integer socket descriptor
 */
int openNewTCPConnection(char* hostname, int port)
{
	sockaddr_in addr;
	hostentry* host = gethostbyname(hostname);													//Resolve hostname

	if (host == NULL)
	{
		fprintf(stderr, "Name lookup for %s failed\n", hostname);
		exit(EXIT_FAILURE);
	}

	int socketDescriptor = socket(AF_INET, SOCK_STREAM, 0);										//Get a new socket object/descriptor
	if (socketDescriptor < 0)
	{
		fprintf(stderr, "Could not create socket\n");
		exit(EXIT_FAILURE);
	}

	memset(&addr, 0, sizeof(addr));																//Zero out the memory pointed to be addr
																								//Important so not to mess up ip addresses/etc from left of memory
	addr.sin_family = AF_INET;
	addr.sin_port = htons(port);
	memcpy(&addr.sin_addr, host->h_addr_list[0], host->h_length);

	if (connect(socketDescriptor, (sockaddr *)&addr, sizeof(addr)) != 0)						//Connect our socket descriptor to the socket address struct
	{
		close(socketDescriptor);
		fprintf(stderr, "Failed to connect TCP stream\n");
		exit(EXIT_FAILURE);
	}
	return socketDescriptor;
}

/*
 *	@name 	openNewTCPListener
 *	@param	int port
 * 	@info	Creates a new tcp listner to listen for connections on port
 *	@return integer socket descriptor
 */
int openNewTCPListener(int port)																//Follows mostly the same structure as openNewTCPConnection
{																								//Only for listening for connectionss instead
	sockaddr_in addr;

	int socketDescriptor = socket(AF_INET, SOCK_STREAM, 0);
	if (socketDescriptor < 0)
	{
		fprintf(stderr, "Could not create socket\n");
		exit(EXIT_FAILURE);
	}

	memset(&addr, 0, sizeof(addr));
	addr.sin_family = AF_INET;
	addr.sin_port = htons(port);
	addr.sin_addr.s_addr = htonl(INADDR_ANY);

	if (bind(socketDescriptor, (sockaddr *)&addr, sizeof(addr)) != 0)							//Reserve this port for us to listen for connections on
	{
		fprintf(stderr, "Cannot bind port %i\n", port);
		exit(EXIT_FAILURE);
	}

	if (listen(socketDescriptor, 10) != 0)
	{
		fprintf(stderr, "Cannot configure port %d for listening!\n", port);
		exit(EXIT_FAILURE);
	}

	return socketDescriptor;
}

/*
 *	@name 	verify_callback
 *	@param	int preverify_ok, X509_STORE_CTX* ctx
 * 	@info	Acts as an additional layer in certificate chain verification
 *			OpenSSL doesn't provide hostname checking by default so this 
 *			callback looks at the common name for the certificate at depth
 *			0 and checks if it exactly matches the hostname we're trying to
 *			establsih a ssl session with.
 *	@return integer success state of verification
 */
static int verify_callback(int preverify_ok, X509_STORE_CTX* ctx)
{
	if (! preverify_ok) return 0;																//Preverify ok contains the verify status of the checks before this one
	if (X509_STORE_CTX_get_error_depth(ctx) != 0) return 1;										//If the previous check as failed we shoudln't override it

	char name[256];																				//Hopefully common names aren't > 256 bytes, cant find any rules about it
	X509_NAME_get_text_by_NID(X509_get_subject_name(X509_STORE_CTX_get_current_cert(ctx)), NID_commonName, name, sizeof(name));
	
	if (strcmp(name, flags.hostName) != 0)
	{
		fprintf(stderr, "Certfificate at depth 0's common name (%s) does not match hostname (%s)\n", name, flags.hostName);
		return 0;
	}

	return 1;
}

/*
 *	@name 	initialiseClientCTX
 *	@param	
 * 	@info	Initialises the client's SSL context
 *	@return struct SSL_CTX* holding the client's ssl context
 */
SSL_CTX* initialiseClientCTX()
{
	SSL_library_init();																			//Needed before anything else happens
	OpenSSL_add_all_algorithms();																//Also needed
	SSL_load_error_strings();																	//Not needed per say, but useful for debugging
	SSL_METHOD* method = SSLv3_client_method();													//SSLv2 has known vulnerabilities
	SSL_CTX* ctx = SSL_CTX_new(method);
	
	SSL_CTX_set_verify(ctx, SSL_VERIFY_PEER, verify_callback);									//Set peer verification and use our additional callback for hostname verification

	if (ctx == NULL)
	{
		ERR_print_errors_fp(stderr);
		exit(EXIT_FAILURE);
	}

	return ctx;
}

/*
 *	@name 	initialiseServerCTX
 *	@param	
 * 	@info	Initialises the server's SSL context
 *	@return struct SSL_CTX* holding the server's ssl context
 */
SSL_CTX* initialiseServerCTX()
{
	SSL_library_init();																			//Explained above
	OpenSSL_add_all_algorithms();
	SSL_load_error_strings();
	SSL_METHOD* method = SSLv3_server_method();
	SSL_CTX* ctx = SSL_CTX_new(method);

	if (ctx == NULL)
	{
		ERR_print_errors_fp(stderr);
		exit(EXIT_FAILURE);
	}

	return ctx;
}

/*
 *	@name 	printCertificates
 *	@param	SSL* ssl
 * 	@info	Prints the certificates of the connected peer
 *			Not critical, only used for debugging/additional information
 *	@return
 */
void printCertificates(SSL* ssl)
{
	X509* certificate = SSL_get_peer_certificate(ssl);

	if (certificate == NULL)
	{
		printf("No certificates\n");
		return;
	}

	printf("Server certificates:\n");
	char* output = X509_NAME_oneline(X509_get_subject_name(certificate), 0, 0);
	printf("Subject: %s\n", output);
	free(output);
	output = X509_NAME_oneline(X509_get_issuer_name(certificate), 0, 0);
	printf("Issuer: %s\n", output);
	free(output);
	X509_free(certificate);
}

/*
 *	@name 	signDigestWithKeyFile
 *	@param	char* digest, char* fileName, unsigned char** signature
 * 	@info	Signs digest with the private key in fileName
 *			Stores the signature in *(char** signature)
 *			Returns the length of the signature
 *	@return unsigned int The length of the signature
 */
unsigned int signDigestWithKeyFile(char* digest, char* fileName, unsigned char** signature)
{
	FILE* filePointer = fopen(fileName, "r");

	if (!filePointer)
	{
		fprintf(stderr, "Unable to open %s.\n", fileName);
		return 0;
	}
	
	RSA* rsa = PEM_read_RSAPrivateKey(filePointer, NULL, NULL, NULL);									//We're assuming a few things here for simplicity, i.e. PEM format, RSA key

	if (!rsa) {
		fprintf(stderr, "Unable to parse Private Key File in: %s\n", fileName);
		fclose(filePointer);
		return 0;
	}
	fclose(filePointer);

	*signature = memAlloc(NULL, 128);																	//Never had a signature larger than 128, if it is, it will get trucated.
	unsigned int size;
	RSA_sign(NID_md5, (unsigned char*)digest, (unsigned int)strlen(digest), *signature, &size, rsa);

	return size;
}

/*
 *	@name 	checkVouch
 *	@param	char* targetDigest, char* sourceDigest, unsigned char* signature, unsigned int sigLength
 * 	@info	Checks the validity of a given signature for a given message digest
 *	@return SERVER_STATE_SUCCESS if the signature is valid,
 *			SERVER_STATE_VOUCH_SOURCE_NOT_FOUND if we don't have the corresponding public key
 *			SERVER_STATE_VOUCH_SIGNATURE_INVALID if the signature is invalid
 */
int checkVouch(char* targetDigest, char* sourceDigest, unsigned char* signature, unsigned int sigLength)
{
	User* user;
	if ((user = getUserFromDigest(sourceDigest)) == NULL) return SERVER_STATE_VOUCH_SOURCE_NOT_FOUND;

	EVP_PKEY* evpKey = X509_get_pubkey(user->x509);
	RSA* rsa = EVP_PKEY_get1_RSA(evpKey);

	return (RSA_verify(NID_md5, 
		(unsigned char*)targetDigest,
		(unsigned int)strlen(targetDigest), 
		signature, 
		sigLength, 
		rsa)) ? SERVER_STATE_SUCCESS : SERVER_STATE_VOUCH_SIGNATURE_INVALID;
}

/*
 *	@name 	fileVouchersFromFileData
 *	@param	FileData* fileData
 * 	@info	Given a FileData struct we can determine who, if anyone has vouched for that file.
 *	@return struct UerTable* UserTable of users that have vouched for the file
 */
UserTable* fileVouchersFromFileData(FileData* fileData)
{
	char* delim = strrchr(fileData->fileName, '/');														//Gets a pointer to the last / and set it to null so fileData->fileName reads as the directories above
	*delim = '\0';

	UserTable* vouchers = memAlloc(NULL, sizeof(UserTable));											//Initialise blank UserTable

	DIR* directoryPointer = opendir(fileData->fileName);												//Open the directory now represented by fileData->fileName
	
	if (directoryPointer == NULL)
	{
		fprintf(stderr, "Could not open %s errno %d\n", USER_FILES_DIRECTORY, errno);
		perror("USER_FILES_DIRECTORY");
		exit(EXIT_FAILURE);
	}

	dirent* directoryEntry;
	while ((directoryEntry = readdir(directoryPointer)) != NULL)										//Loop through the entries in the directory
	{
		if (directoryEntry->d_name[0] == '.') continue;													//Skip hidden files and '.' '..'
		
		int fileNameLength = strlen(directoryEntry->d_name);
				
		if (strcmp(directoryEntry->d_name + fileNameLength - 4, ".sig") != 0) continue;					//If the file doesn't end in .sig skip it
		
		char* fileName = memAlloc(NULL, strlen(fileData->fileName) + fileNameLength + 2);
		strcat(fileName, fileData->fileName);
		strcat(fileName, "/");
		strcat(fileName, directoryEntry->d_name);
		
		FileData* signature = readData(fileName);

		*strrchr(directoryEntry->d_name, '.') = '\0';													//The filename minus the extension is the digest of the user who signature this is
		*strrchr(fileName, '/') = '\0';																	//The upper directory of this file's location is it's digest (Saves recomputing it)
																										//Check that the signature is valid
		if (checkVouch(strchr(fileName, '/')+1, directoryEntry->d_name, (unsigned char*)signature->data, (unsigned int)signature->size) == SERVER_STATE_SUCCESS)
		{
			vouchers->users = memAlloc(vouchers->users, (++vouchers->count)*sizeof(User));				//If it is, add this user to the list of vouchers
			vouchers->users[vouchers->count-1] = getUserFromDigest(directoryEntry->d_name);
		}

		free(fileName);
		FILEDATA_destoryFileData(signature);
	}

	closedir(directoryPointer);
	*delim = '/';																						//Restore fileData->fileName before returning back;
	return vouchers;
}

/*
 *	@name 	loadCertificateFromFile
 *	@param	char* fileName
 * 	@info	Loads an X509 certificate from a file and returns a pointer to it if valid
 *	@return struct X509* x509 certificate from the file
 */
X509* loadCertificateFromFile(char* fileName)
{
	FILE* filePointer = fopen(fileName, "r");

	if (!filePointer)
	{
		fprintf(stderr, "Unable to open %s.\n", fileName);
		return NULL;
	}
	
	X509* certificate = PEM_read_X509(filePointer, NULL, NULL, NULL);
	if (!certificate) {
		fprintf(stderr, "Unable to parse certificate in: %s\n", fileName);
		fclose(filePointer);
		return NULL;
	}
	fclose(filePointer);

	return certificate;
}

/*
 *	@name 	verifySelfSignedCertificate
 *	@param	char* X509* cert
 * 	@info	Verifies that a self signed certificate is valid (e.g. right dates, etc)
 *	@return struct X509* x509 certificate from the file
 */
int verifySelfSignedCertificate(X509* cert)
{
	X509_STORE* store = X509_STORE_new();
	X509_STORE_CTX* ctx = X509_STORE_CTX_new();

	X509_STORE_add_cert(store, cert);
	
	X509_STORE_CTX_init(ctx, store, cert, NULL);
	int valid = X509_verify_cert(ctx);
	
	X509_STORE_CTX_free(ctx);
	X509_STORE_free(store);

	return valid;
}

/*
 *	@name 	SSL_writeFileData
 *	@param	SSL* ssl, FileData* fileData
 * 	@info	Wrapper for writing a whole FileData struct to a SSL session.
 *	@return 
 */
void SSL_writeFileData(SSL* ssl, FileData* fileData)
{
	int fileNameLength = (int)strlen(fileData->fileName);
	chunkedSSL_write(ssl, &fileNameLength, sizeof(int));
	
	chunkedSSL_write(ssl, fileData->fileName, fileNameLength);
	
	chunkedSSL_write(ssl, &fileData->size, sizeof(int));
	
	chunkedSSL_write(ssl, fileData->data, fileData->size);
}

/*
 *	@name 	SSL_readFileData
 *	@param	SSL* ssl, FileData* fileData
 * 	@info	Wrapper for reading a whole FileData struct from a SSL session.
 *	@return 
 */
FileData* SSL_readFileData(SSL* ssl)
{
		FileData* fileData = FILEDATA_createFileData();

		int fileNameLength;
		chunkedSSL_read(ssl, &fileNameLength, sizeof(int));

		fileData->fileName = memAlloc(NULL, fileNameLength + 1);
		chunkedSSL_read(ssl, fileData->fileName, fileNameLength);

		chunkedSSL_read(ssl, &fileData->size, sizeof(int));

		fileData->data = memAlloc(NULL, fileData->size);
		chunkedSSL_read(ssl, fileData->data, fileData->size);

		return fileData;
}

/********************************************************************************************************************************************
 * These are wrappers around SSL_write and SSL_read 																						*
 *																																			*
 * SSL only sends/recieves 16384 (16*1024) bytes at a time. So you need to chunk larger data.												*
 * 																																			*
 * Glad I spent hours trying to figure out why only some files were corrupted after transfer since this isn't anywhere in the documentation.*
 * 											Implying OpenSSL has documentation																*
 ********************************************************************************************************************************************

 *
 *	@name 	chunkedSSL_write
 *	@param	SSL* ssl, void* buf, int size
 * 	@info	Wrappers around SSL_write
 *	@return 
 */
int chunkedSSL_write(SSL* ssl, void* buf, int size)
{
	int written = 0;
	while (written < size) written += SSL_write(ssl, (char*)buf+written, size); //SSL_read reads in bytes and in C99 sizeof(char) is always 1. Becuase void pointer arithmetic is a no no.

	return (written == size) ? 1 : 0;
}

/*
 *	@name 	chunkedSSL_read
 *	@param	SSL* ssl, void* buf, int size
 * 	@info	Wrappers around SSL_read
 *	@return 
 */
int chunkedSSL_read(SSL* ssl, void* buf, int size)
{
	int read = 0;
	while (read < size) read += SSL_read(ssl, (char*)buf+read, size);

	return (read == size) ? 1 : 0;
}
