/*
	CITS3002 Project 2014
	Name:				Dylan R. Johnston
	Student Number:		20770146
	Date: 				15/05/2014
*/
#include "trustcloud.h"

/*
 *	@name 	depthFirstSearch
 *	@param	Matrix* edgeMatrix, int* visited, int currentNode, int targetNode, int chainLength, int minChainLength
 * 	@info	Performs a recursive depth first search on edgeMatrix, looking for a non intersecting path from currentNode
 *			back to target node of at least minChainLength.
 *	@return int Length of the first path found of at least minChainLength. Not the longest possible.
 */
int depthFirstSearch(Matrix* edgeMatrix, int* visited, int currentNode, int targetNode, int chainLength, int minChainLength)
{	
	if (currentNode == targetNode && chainLength >= minChainLength) return chainLength; 									//Found a solution, return back

	for (int i = 0; i < edgeMatrix->length; i++)																			//For each possible connection of this node
	{
		if (edgeMatrix->data[currentNode][i] == 1 && visited[i] == 0)														//If there is a connection and it hasn't been visisted yet on this path
		{
			visited[i] = 1;																									//Mark as visited
			int length = depthFirstSearch(edgeMatrix, visited, i, targetNode, chainLength+1, minChainLength);				//Call a depth first search from this new node back to targetNode

			if (length != -1) return length;																				//If depth first search from that node was successfull in finding a path back to targetNode
																															//Return the length of the path
			visited[i] = 0;																									//Else mark this node as unvisited and try a new connectiion
		}																													//This is needed becuase visiting a node along one possible path is independant of another path
																															//The visited table is to stop the path from being intersecting
	}
	return -1;
}

/*
 *	@name 	doesExistLargeEnoughRingForNode
 *	@param	Matrix* edgeMatrix, int node, int minChainLength
 * 	@info	Uses depthFirstSearch to find if there exists a path back to node from node of at least minChainLength
 *	@return int Length of the first path found of at least minChainLength. Not the longest possible.
 */
int doesExistLargeEnoughRingForNode(Matrix* edgeMatrix, int node, int minChainLength)
{
	int* visited = memAlloc(NULL, edgeMatrix->length*sizeof(int));
	int length = depthFirstSearch(edgeMatrix, visited, node, node, 0, minChainLength);
	free(visited);
	return length;
}
