/*
	CITS3002 Project 2014
	Name:				Dylan R. Johnston
	Student Number:		20770146
	Date: 				15/05/2014
*/
#include "trustcloud.h"

/*
 *	@name 	MATRIX_createMatrix
 *	@param	int length
 * 	@info	Creates a square matrix of side lgnth
 *	@return struct Matrx* the empty square matrix
 */
Matrix* MATRIX_createMatrix(int length)
{
	Matrix* matrix = memAlloc(NULL, sizeof(Matrix)); 	//Allocate the space for pointers to the data and its size

	matrix->data = memAlloc(NULL, length*sizeof(int*));	//Allocate the space for the vertical column of pointers to int* rows

	for (int i = 0; i < length; i++)
	{
		matrix->data[i] = memAlloc(NULL, length*sizeof(int)); //Allocate the space for the rows.
	}

	matrix->length = length;

	return matrix;
}

/*
 *	@name 	Matrix_addNewRowColumn
 *	@param	Matrix* matrix, int i
 * 	@info	Inserts a new blank row/column into Matrix at i
 *	@return struct Matrx* expanded
 */
Matrix* Matrix_addNewRowColumn(Matrix* matrix, int i)
{
	if (matrix == NULL)
	{
		return MATRIX_createMatrix(1);
	}

	Matrix* newMatrix = MATRIX_createMatrix(matrix->length + 1); //It's difficult to expand 2d memory structures so just grab a new one.

	for (int j = 0; j < matrix->length; j++)
	{
		for (int k = 0; k < matrix->length; k++)
		{
			newMatrix->data[j+((j>=i)?1:0)][k+((k>=i)?1:0)] = matrix->data[j][k]; //The ((j>=i)?1:0) and ((k>=i)?1:0 create small offsets that cause the loop to skip over I when copying the new array
		}
	}

	MATRIX_destroyMatrix(matrix);
	return newMatrix;
}
/*
 *	@name 	MATRIX_destroyMatrix
 *	@param	void
 * 	@info	Properly frees the matrix stuct
 *	@return
 */

void MATRIX_destroyMatrix(Matrix* matrix)
{
	for (int i = 0; i < matrix->length; i++)
	{
		free(matrix->data[i]);
	}

	free(matrix->data);
	free(matrix);
}
