/*
	CITS3002 Project 2014
	Name:				Dylan R. Johnston
	Student Number:		20770146
	Date: 				15/05/2014
*/
#include "trustcloud.h"


/*
 *	@name 	FILEDATA_createFileData
 *	@param	
 * 	@info	"Creates a new filData"
 *	@return struct FileData* the empty FileName
 */
FileData* FILEDATA_createFileData()
{
	return (FileData *)memAlloc(NULL, sizeof(FileData));
}

/*
 *	@name 	FILEDATA_createFileData
 *	@param	
 * 	@info	"FILEDATA_destoryFileData"
 *	@return struct FileData* the empty FileName
 */
void FILEDATA_destoryFileData(FileData* fileData)
{
	free(fileData->fileName);
	free(fileData->data);
	free(fileData);
}

/*
 *	@name 	writeData
 *	@param	FileData* fileData
 * 	@info	Writes out fileData according to the structure
 *	@return struct FileData* the empty FileName
 */
int writeData(FileData* fileData)
{
	FILE* fileOut = fopen(fileData->fileName, "w");

	if(fileOut == NULL)
	{
		fprintf(stderr, "Error, errno %i\n", errno);
		perror("ERROR OPENING OUTPUT FILE STREAM");
		return 0;
	}

	if ((int)fwrite(fileData->data, 1, fileData->size, fileOut) != fileData->size)
	{
		fprintf(stderr, "Error, errno %i\n", errno);
		perror("ERROR WRITING FILE");
		fclose(fileOut);
		return 0;
	}

	fclose(fileOut);

	return 1;
}

/*
 *	@name 	readData
 *	@param	char* 
 * 	@info	Reads in fileData according to the structure
 *	@return struct FileData* the file's Data
 */
FileData* readData(char* inputFilename)
{
	FILE* fileIn = fopen(inputFilename, "r");

	if(fileIn == NULL)
	{
		fprintf(stderr, "Error, errno %i\n", errno);
		perror("ERROR OPENING INPUT FILE STREAM");
		exit(EXIT_FAILURE);
	}

	FileData* fileData = FILEDATA_createFileData();

	fileData->fileName = memAlloc(NULL, strlen(inputFilename)+1);
	strcpy(fileData->fileName, inputFilename);

	fseek(fileIn, 0, SEEK_END);
	fileData->size = (int)ftell(fileIn);									
	rewind(fileIn);

	fileData->data = memAlloc(NULL, fileData->size);

	if ((int)fread(fileData->data, 1, fileData->size, fileIn) != fileData->size)
	{
		fprintf(stderr, "Error, errno %i\n", errno);
		perror("ERROR READING FILE");
		exit(EXIT_FAILURE);
	}

	fclose(fileIn);
	return fileData;
}

/*
 *	@name 	fileDataFromDigest
 *	@param	char* md5Digest
 * 	@info	Returns the FileData struct of a file with a corresponding digest
 *	@return struct FileData* 
 */
FileData* fileDataFromDigest(char* md5Digest)
{
	DIR* directoryPointer = opendir(USER_FILES_DIRECTORY);
	
	if (directoryPointer == NULL)
	{
		fprintf(stderr, "Could not open %s errno %d\n", USER_FILES_DIRECTORY, errno);
		perror("USER_FILES_DIRECTORY");
		exit(EXIT_FAILURE);
	}

	dirent* directoryEntry;
	while ((directoryEntry = readdir(directoryPointer)) != NULL)
	{
		if (strcmp(directoryEntry->d_name, md5Digest) == 0)
		{
			char* filePath = memAlloc(NULL, strlen(directoryEntry->d_name) + strlen(USER_FILES_DIRECTORY) + 2); //Extra for NULL Byte and /
			strcat(filePath, USER_FILES_DIRECTORY);
			strcat(filePath, "/");
			strcat(filePath, directoryEntry->d_name);
			
			closedir(directoryPointer);
			directoryPointer = opendir(filePath);

			while ((directoryEntry = readdir(directoryPointer)) != NULL)
			{
				if (directoryEntry->d_name[0] == '.') continue;

				int fileNameLength = strlen(directoryEntry->d_name);
				if (strcmp(directoryEntry->d_name + fileNameLength - 4, ".sig") == 0) continue;
				
				filePath = memAlloc(filePath, strlen(filePath) + fileNameLength + 2);
				strcat(filePath, "/");
				strcat(filePath, directoryEntry->d_name);
				FileData* fileData = readData(filePath);
				free(filePath);
				closedir(directoryPointer);
				return fileData;
			}

			closedir(directoryPointer);
			free(filePath);
		}
	}

	return NULL;
}

/*
 *	@name 	createDirectory
 *	@param	char* dir
 * 	@info	Creates a new directory, doesn't throw error if directory already exists
 *	@return int error cod
 */
int createDirectory(char* dir)
{
	if ( !(mkdir(dir, (mode_t) 0777) == 0 || errno == 17) )	//mkdir will fail if the directory already exists, this sets errno to 17, for our purposeses this is ok and do not quit.
	{
		fprintf(stderr, "Error, errno %i\n", errno);
		perror("ERROR CREATING DIRECTORY");
		return 0;
	}
	return 1;
}

/*
 *	@name 	writeOutUserFile
 *	@param	fileData* fileData
 * 	@info	Wrties out a FileData struct to the right place on the server
 *	@return int error code
 */
int writeOutUserFile(FileData* fileData)
{
	char* md5 = messageDigest(fileData->data, fileData->size);
	char* fileDirectory = memAlloc(NULL, strlen(USER_FILES_DIRECTORY) + EVP_MAX_MD_SIZE / 2 + 2); //Extra for NULL byte and forward slash
	strcpy(fileDirectory, USER_FILES_DIRECTORY);
	strcat(fileDirectory, "/");
	strcat(fileDirectory, md5);
	free(md5);

	if (!createDirectory(USER_FILES_DIRECTORY))
	{
		free(fileDirectory);
		return 0;
	}
	
	if (!createDirectory(fileDirectory))
	{
		free(fileDirectory);
		return 0;
	}

	char* fullFileName = memAlloc(fileDirectory, strlen(fileDirectory) + strlen(fileData->fileName) + 2);
	strcat(fullFileName, "/");
	strcat(fullFileName, fileData->fileName);
	free(fileData->fileName);
	fileData->fileName = fullFileName;

	return writeData(fileData);
}

/*
 *	@name 	writeOutCertificate
 *	@param	fileData* fileData
 * 	@info	Wrties out a FileData struct of a certificat  to the right place on the server. Add the user to the globalUserTable.
 *	@return int error code
 */
int writeOutCertificate(FileData* fileData)
{
	char* md5 = messageDigest(fileData->data, fileData->size);
	char* filePath = memAlloc(NULL, strlen(USER_CERTS_DIRECTORY) + EVP_MAX_MD_SIZE / 2 + 2); //Extra for NULL byte and forward slash
	strcpy(filePath, USER_CERTS_DIRECTORY);
	strcat(filePath, "/");
	strcat(filePath, md5);
	free(md5);

	if (!createDirectory(USER_CERTS_DIRECTORY))
	{
		free(filePath);
		return 0;
	}

	fileData->fileName = filePath;

	if(!writeData(fileData)) return 0;

	User* user = USER_createUser();
	user->x509 = loadCertificateFromFile(filePath);;
	user->certificateDigest = certificateDigest(user->x509);

	addUserToGlobalUserTable(user);

	return 1;
}


