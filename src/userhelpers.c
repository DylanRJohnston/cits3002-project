#include "trustcloud.h"

/*
 *	@name 	USER_createUser
 *	@param	
 * 	@info	Creates a new User struct
 *	@return struct User* the new user struct.
 */
User* USER_createUser()
{
	User* user = memAlloc(NULL, sizeof(User));
	return user;
}

/*
 *	@name 	USER_destroyUser
 *	@param	User* user
 * 	@info	Properly frees a user struct
 *	@return
 */
void USER_destroyUser(User* user)
{
	free(user->certificateDigest);
	X509_free(user->x509);
}

/*
 *	@name 	addUserToGlobalUserTable
 *	@param	User* user
 * 	@info	Adds a new user to the globalUserTable, takes care of updating the globalTrustMatrix
 *			to reflect the insertion of a new user, movement of old users' indexes.
 *	@return
 */
void addUserToGlobalUserTable(User* user)
{
	int i;
	for (i = 0; i < globalUserTable.count; i++)																	//Global user table sort of acts like a priority list
	{																											//Where the certificateDigest is used to alphanumerically sort digests
		int cmp = strcmp(user->certificateDigest, globalUserTable.users[i]->certificateDigest);

		if (cmp == 0)                                                                              				 //Users are uniquely identified via their certificateDigest if cmp == 0, the user already exists in the table
		{
			return;
		}
		else if (cmp < 0)																						//The user i is the first user alphanumerically greater than the new user
		{
			break;
		}
	}

	globalUserTable.count++;																					//Grow the globalUserTable count
	globalUserTable.users = memAlloc(globalUserTable.users, sizeof(User) * globalUserTable.count);				//Reallocate the globalUserTable.users array to bigger memory
	memcpy(globalUserTable.users+i+1, globalUserTable.users+i, sizeof(User) * (globalUserTable.count - i - 1)); //Move old users from i to the end up the array up 1

	globalUserTable.users[i] = user;																			//Insert new user into i

	globalTrustMatrix = Matrix_addNewRowColumn(globalTrustMatrix, i);											//Insert a new row/column into globalTrustMatrix at i
}


/*
 *	@name 	getUserFromDigest
 *	@param	char* digest
 * 	@info	Find the User struct matching the given digest
 * 			Uses getUserIndexFromDigest which uses a binary search since globalUserTable is ordered
 *	@return struct User* the user with matching digest or NULL if it does not exist
 */
User* getUserFromDigest(char* digest)
{
	int index = getUserIndexFromDigest(digest);
	return (index != -1) ? globalUserTable.users[index] : NULL;
}

/*
 *	@name 	getUserIndexFromDigest
 *	@param	char* digest
 * 	@info	Returns the index of the User from globalUserTable with the digest matching the given digest
 *			Since globalUserTable is ordered it uses a binary search.
 *	@return int The index in globalUserTable of the matching user or -1 if it does not exist
 */
int getUserIndexFromDigest(char* digest)
{
	int min = 0;
	int max = globalUserTable.count - 1;

	while(max >= min)					
	{
		int mid = (max + min) / 2;
		int cmp = strcmp(globalUserTable.users[mid]->certificateDigest, digest);

		if (cmp == 0)
		{
			return mid;
		}
		else if(cmp < 0)															//cmp < 0 means the matching user is in the upper half of the current range [min, max]
		{
			 min = mid + 1;
		}
		else if(cmp > 0)															//cmp > 0 means the matching user is in the lower half of the current range [min, max]
		{
			max = mid - 1;
		}

	}
	
	return -1;
}

/*
 *	@name 	loadUsers
 *	@param	
 * 	@info	Loads users from disk into the globalUserTable
 *	@return
 */
void loadUsers()
{
	createDirectory(USER_CERTS_DIRECTORY);
	DIR* directoryPointer = opendir(USER_CERTS_DIRECTORY);

	if (directoryPointer == NULL)
	{
		fprintf(stderr, "Could not open %s errno %d\n", USER_CERTS_DIRECTORY, errno);
		perror("USER_CERTS_DIRECTORY");
		exit(EXIT_FAILURE);
	}

	dirent* directoryEntry;
	while ((directoryEntry = readdir(directoryPointer)) != NULL)											//For each file in USER_CERTS_DIRECTORY
	{
		if (directoryEntry->d_name[0] == '.') continue;														//Skip hidden files, '.' & '..'

		char* fileName = memAlloc(NULL, strlen(USER_CERTS_DIRECTORY) + strlen(directoryEntry->d_name) + 2); //Extra for null byte and /
		strcat(fileName, USER_CERTS_DIRECTORY);
		strcat(fileName, "/");
		strcat(fileName, directoryEntry->d_name);

		X509* cert = loadCertificateFromFile(fileName);														//Try to load a certificate from the file

		if (cert == NULL)
		{
			fprintf(stderr, "Could not load certificate %s\n", fileName);
			free(fileName);
			continue;
		}

		if (verifySelfSignedCertificate(cert) != 1)															//Verify that the self signed certificate is valid
		{
			fprintf(stderr, "Not a valid certificate %s.\n", fileName);
			free(fileName);
			continue;
		}

		User* user = USER_createUser();																		//Create a new user from the certificate
		user->x509 = cert;
		user->certificateDigest = certificateDigest(user->x509);

		addUserToGlobalUserTable(user);																		//Add the new user to the globalUserTable
	}
}

/*
 *	@name 	loadGlobalTrustMatrix
 *	@param	
 * 	@info	Loads the globalTrustMatrix from disk
 *	@return
 */
void loadGlobalTrustMatrix()
{
	createDirectory(USER_TRUST_DIRECTORY);
	DIR* directoryPointer = opendir(USER_TRUST_DIRECTORY);

	if (directoryPointer == NULL)
	{
		fprintf(stderr, "Could not open %s errno %d\n", USER_TRUST_DIRECTORY, errno);
		perror("USER_TRUST_DIRECTORY");
		exit(EXIT_FAILURE);
	}

	dirent* directoryEntry;
	while ((directoryEntry = readdir(directoryPointer)) != NULL)											//For each file in USER_TRUST_DIRECTORY
	{
		if (directoryEntry->d_name[0] == '.') continue;
		char* filePath = memAlloc(NULL, strlen(directoryEntry->d_name) + strlen(USER_TRUST_DIRECTORY) + 2); //Extra for NULL Byte and /
		strcat(filePath, USER_TRUST_DIRECTORY);
		strcat(filePath, "/");
		strcat(filePath, directoryEntry->d_name);
		
		DIR* subDirectoryPointer = opendir(filePath);
		if (subDirectoryPointer == NULL)
		{
			fprintf(stderr, "Could not open %s errno %d\n", USER_TRUST_DIRECTORY, errno);
			perror("USER_TRUST_DIRECTORY");
			exit(EXIT_FAILURE);
		}

		dirent* subDirectoryEntry;
		while ((subDirectoryEntry = readdir(subDirectoryPointer)) != NULL)									//For each file in each file in USER_TRUST_DIRECTORY
		{
			if (subDirectoryEntry->d_name[0] == '.') continue;
			char* fullFilePath = memAlloc(NULL, strlen(subDirectoryEntry->d_name) + strlen(filePath) + 2);	//The structure of this directory is USER_TRUST_DIRECTORY/USER_BEING_VOUCHED_FOR/USER_DOING_THE_VOUCHING
			strcat(fullFilePath, filePath);
			strcat(fullFilePath, "/");
			strcat(fullFilePath, subDirectoryEntry->d_name);
			FileData* fileData = readData(fullFilePath);

			if(checkVouch(directoryEntry->d_name, subDirectoryEntry->d_name, (unsigned char*) fileData->data, (unsigned int)fileData->size) == SERVER_STATE_SUCCESS)
			{
				globalTrustMatrix->data[getUserIndexFromDigest(subDirectoryEntry->d_name)][getUserIndexFromDigest(directoryEntry->d_name)] = 1;
			}
			else
			{
				fprintf(stderr, "Found trust file but has invalid signature or users don't exit %s -> %s\n", subDirectoryEntry->d_name, directoryEntry->d_name);
			}
			free(fullFilePath);
		}

		closedir(subDirectoryPointer);
		free(filePath);
	}

	closedir(directoryPointer);
}
