/*
	CITS3002 Project 2014
	Name:				Dylan R. Johnston
	Student Number:		20770146
	Date: 				15/05/2014
*/
#include "trustcloud.h"

/*
* 	@name 	memAlloc
* 	@param	void* ptr, size_t size
* 	@info 	A simple memory allocation wrapper that checks for sucessful memory allocation,
			also provides clean memory if a NULL ptr is provided.
	@return	Returns a pointer to the allocated memory block.
*/
void* memAlloc(void* ptr, size_t size)
{
	if (ptr == NULL)
	{
		ptr = calloc((int)size, (size_t)1);  //Allocate free and clean memory
	}
	else
	{
		ptr = realloc(ptr, size);			//Reallocate 
	}

	if (ptr == NULL) 
	{
		fprintf(stderr, "MEMORY ALLOCATION FAILED\n");
		exit(EXIT_FAILURE);
	}

	return ptr;
}
