/*
	CITS3002 Project 2014
	Name:				Dylan R. Johnston
	Student Number:		20770146
	Date: 				15/05/2014
*/
#pragma clang diagnostic ignored "-Wdeprecated-declarations" //Needed to compile on Mac OS X 10.7+, Apple deprecated OpenSSL

#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <openssl/ssl.h>
#include <openssl/err.h>
#include <netdb.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>
#include <netinet/in.h>
#include <arpa/inet.h>

typedef enum Operations
{
	ADD = 'a',
	FETCH = 'f',
	LIST = 'l',
	VOUCHFILE = 'v',
	VOUCHUSER = 'V',
	UPLOAD = 'u',
	SERVER = 's'
} Operations;

typedef enum SERVER_STATE
{
	SERVER_STATE_FAILURE,
	SERVER_STATE_SUCCESS,
	SERVER_STATE_FILE_EXISTS_AND_TRUSTED,
	SERVER_STATE_FILE_NOT_TRUSTED,
	SERVER_STATE_FILE_NOT_FOUND,
	SERVER_STATE_FILE_TAMPERED,
	SERVER_STATE_VOUCH_TARGET_NOT_FOUND,
	SERVER_STATE_VOUCH_SOURCE_NOT_FOUND,
	SERVER_STATE_VOUCH_SIGNATURE_INVALID
} SERVER_STATE;

typedef struct Flags
{
	char* fileName;
	char* sourceDigest;
	char* targetDigest;
	char* hostName;
	int port;
	int requiredTrustCircumference;
	Operations operation; //Can be a, f, l, v, u, or s. (Add file, fetch file, list files, vouch for file, upload cert, or server respectivly)
} Flags;

typedef struct FileData
{
	char* fileName;
	char* data;
	int size;
} FileData;

typedef struct Matrix
{
	int** data;
	int length;
} Matrix;

typedef struct User
{
	char* certificateDigest;
	X509* x509;
} User;

typedef struct UserTable
{
	User** users;
	int count;
} UserTable;

typedef struct hostent hostentry;
typedef struct sockaddr_in sockaddr_in;
typedef struct sockaddr sockaddr;
typedef struct dirent dirent;

//Global Structures
extern Flags flags;
extern Matrix* globalTrustMatrix;
extern UserTable globalUserTable;

//GLOBAL CONSTANTS
#define ROOT_CERTIFICATE_AUTHORITY_FILE "rootCA.crt"
#define SERVER_CERTIFICATE              "server.crt"
#define SERVER_PRIVATE_KEY              "server.key"
#define USER_FILES_DIRECTORY            "UserFiles"
#define USER_CERTS_DIRECTORY            "UserCerts"
#define USER_TRUST_DIRECTORY            "UserTrusts"

//sslHelpers
void         loadServerCertificates(SSL_CTX* ctx, char* certificateFile, char* keyFile);
int          openNewTCPConnection(char* hostname, int port);
int          openNewTCPListener(int port);
SSL_CTX*     initialiseClientCTX();
SSL_CTX*     initialiseServerCTX();
void         printCertificates(SSL* ssl);
int          chunkedSSL_write(SSL* ssl, void* buf, int size);
int          chunkedSSL_read(SSL* ssl, void* buf, int size);
void         SSL_writeFileData(SSL* ssl, FileData* fileData);
FileData*    SSL_readFileData(SSL* ssl);
UserTable*   fileVouchersFromFileData(FileData* fileData);
X509*        loadCertificateFromFile(char* fileName);
int          verifySelfSignedCertificate(X509* cert);
int          checkVouch(char* targetDigest, char* sourceDigest, unsigned char* signature, unsigned int sigLength);
unsigned int signDigestWithKeyFile(char* digest, char* fileName, unsigned char** signature);

//matrixHelpers
Matrix* MATRIX_createMatrix(int length);
void    MATRIX_destroyMatrix(Matrix* matrix);
Matrix* Matrix_addNewRowColumn(Matrix* matrix, int index);

//ringdetection
int depthFirstSearch(Matrix* edgeMatrix, int* visited, int currentNode, int targetNode, int chainLength, int minChainLength);
int doesExistLargeEnoughRingForNode(Matrix* edgeMatrix, int node, int minChainLength);

//client
SSL* startClient();
void uploadFileToServer();
void fetchFileFromServer();
void vouchForUser();
void vouchForFile();
void uploadCertificate();
void getListFromServer();

//server
void startServer();

//memoryHelpers
void* memAlloc(void* ptr, size_t size);

//fileIO
FileData* FILEDATA_createFileData();
void      FILEDATA_destoryFileData(FileData* fileData);
FileData* readData(char* inputFilename);
int       writeData(FileData* fileData);
int       writeOutUserFile(FileData* fileData);
int       writeOutCertificate(FileData* fileData);
FileData* fileDataFromDigest(char* md5Digest);
int       createDirectory(char* dir);

//messagedigest
char* certificateDigest(X509* cert);
char* messageDigest(void* buf, int size);
int   verifyDigest(void* buf, int size, char* digest);

//userhelpers
User* USER_createUser();
void  USER_destroyUser(User* user);
void  addUserToGlobalUserTable(User* user);
void  loadUsers();
User* getUserFromDigest(char* digest);
void  loadGlobalTrustMatrix();
int   getUserIndexFromDigest(char* digest);

