/*
	CITS3002 Project 2014
	Name:				Dylan R. Johnston
	Student Number:		20770146
	Date: 				15/05/2014
*/
#include "trustcloud.h"

/*
 *	@name 	startClient
 *	@param	
 * 	@info	Sets up all the necessary file pointers and certificate locations to start the client
 *	@return A struct SSL* representing the initialised SSL connection. 
 */
SSL* startClient()
{
	SSL_CTX* ctx = initialiseClientCTX();
	
	if (SSL_CTX_load_verify_locations(ctx, ROOT_CERTIFICATE_AUTHORITY_FILE, NULL) != 1)
	{
		ERR_print_errors_fp(stderr);
		fprintf(stderr, "FAILED TO LOAD ROOT CERTIFICATE AUTHORITY %s\n", ROOT_CERTIFICATE_AUTHORITY_FILE);
	}

	int socketDescriptor = openNewTCPConnection(flags.hostName, flags.port);
	SSL* ssl = SSL_new(ctx);
	SSL_set_fd(ssl, socketDescriptor);

	if (SSL_connect(ssl) != 1)
	{
		ERR_print_errors_fp(stderr);
		fprintf(stderr, "SSL_CONNECT FAILED\n");
		exit(EXIT_FAILURE);
	}
	else
	{
		return ssl;
	}
}

/*
 *	@name 	closeClient
 *	@param	SSL* ssl
 * 	@info	Closes a given SSL connection via its struct
 *	@return  
 */
void closeClient(SSL* ssl)
{
	int socketDescriptor = SSL_get_fd(ssl);
	SSL_CTX* ctx = SSL_get_SSL_CTX(ssl);
	SSL_free(ssl);
	close(socketDescriptor);
	SSL_CTX_free(ctx);
}

/*
 *	@name 	vouchForUser
 *	@param	
 * 	@info	Contains the protocol code for vouching for another user for the circle of trust.
 *			Uses the global variable flags as its run time parameters
 *	@return 
 */
void vouchForUser()
{
	SSL* ssl = startClient();

	Operations operation = VOUCHUSER;																//The start of every protocol is an Operations enum.
	chunkedSSL_write(ssl, &operation, sizeof(Operations));

	chunkedSSL_write(ssl, flags.targetDigest, EVP_MAX_MD_SIZE / 2);									//The digests are always EVP_MAX_MD_SIZE / 2 bytes when stored in hexidecimal ascii
	chunkedSSL_write(ssl, flags.sourceDigest, EVP_MAX_MD_SIZE / 2);
	
	unsigned char* signature = NULL;
	unsigned int length = signDigestWithKeyFile(flags.targetDigest, flags.fileName, &signature);	//We generate a signature of the target users certificate digest using our private key
	
	chunkedSSL_write(ssl, &length, sizeof(int));
	chunkedSSL_write(ssl, signature, length);
	
	free(signature);																				//Try to remember to free up as much as possible, need to run through a static analyzer

	SERVER_STATE serverState;																		//After we've sent our data the server should respond with a State ENUM indicating the success/failure of our request
	chunkedSSL_read(ssl, &serverState, sizeof(SERVER_STATE));

	switch (serverState)
	{
		case SERVER_STATE_SUCCESS:
			printf("Vouch was successfull\n");
			break;

		case SERVER_STATE_VOUCH_TARGET_NOT_FOUND:
			fprintf(stderr, "Target user not found with digest %s\n", flags.targetDigest);
			closeClient(ssl);
			exit(EXIT_FAILURE);
			break;

		case SERVER_STATE_VOUCH_SOURCE_NOT_FOUND:
			fprintf(stderr, "Source user (you) user not found with digest %s\n", flags.sourceDigest);
			closeClient(ssl);
			exit(EXIT_FAILURE);
			break;

		case SERVER_STATE_VOUCH_SIGNATURE_INVALID:
			fprintf(stderr, "Users found by supplied signature invalid\n");
			closeClient(ssl);
			exit(EXIT_FAILURE);
			break;

		default:
			fprintf(stderr, "Server responded with incorrect state %d.\n", serverState);			//The server code only responds with the above ENUMS to a VOUCHUSER operation so all other responses mean something is weird
			closeClient(ssl);
			exit(EXIT_FAILURE);
			break;
	}
	closeClient(ssl);
}

/*
 *	@name 	vouchForFile
 *	@param	
 * 	@info	Contains the protocol code for vouching for a file stored on the server.
 *			Uses the global variable flags as its run time parameters
 *	@return 
 */
void vouchForFile()
{
	SSL* ssl = startClient();

	Operations operation = VOUCHFILE;
	chunkedSSL_write(ssl, &operation, sizeof(Operations));

	chunkedSSL_write(ssl, flags.targetDigest, EVP_MAX_MD_SIZE / 2);
	chunkedSSL_write(ssl, flags.sourceDigest, EVP_MAX_MD_SIZE / 2);

	unsigned char* signature = NULL;
	unsigned int length = signDigestWithKeyFile(flags.targetDigest, flags.fileName, &signature);	//Generate a signature of the file's digest using our private key

	chunkedSSL_write(ssl, &length, sizeof(int));
	chunkedSSL_write(ssl, signature, length);

	free(signature);

	SERVER_STATE serverState;
	chunkedSSL_read(ssl, &serverState, sizeof(SERVER_STATE));										//Check for the result of our operation request to the server

	switch (serverState)
	{
		case SERVER_STATE_SUCCESS:
			printf("Vouch was successfull\n");
			break;

		case SERVER_STATE_VOUCH_TARGET_NOT_FOUND:
			fprintf(stderr, "Target file not found with digest %s\n", flags.targetDigest);
			closeClient(ssl);
			exit(EXIT_FAILURE);
			break;

		case SERVER_STATE_VOUCH_SOURCE_NOT_FOUND:
			fprintf(stderr, "Source user (you) user not found with digest %s\n", flags.sourceDigest);
			closeClient(ssl);
			exit(EXIT_FAILURE);
			break;

		case SERVER_STATE_VOUCH_SIGNATURE_INVALID:
			fprintf(stderr, "User and file found but supplied signature invalid\n");
			closeClient(ssl);
			exit(EXIT_FAILURE);
			break;

		default:
			fprintf(stderr, "Server responded with incorrect state %d.\n", serverState);
			closeClient(ssl);
			exit(EXIT_FAILURE);
			break;
	}
	closeClient(ssl);
}

/*
 *	@name 	uploadCertificate
 *	@param	
 * 	@info	Contains the protocol code for uploading a certificate to the server.
 *			Uses the global variable flags as its run time parameters
 *			Shares a lot of code with uploadFileToServer but they're subtlety different 
 *	@return 
 */
void uploadCertificate()
{
	FileData* fileData = readData(flags.fileName);													//Read the certificate into a FileData struct
	
	SSL* ssl = startClient();

	Operations operation = UPLOAD;
	chunkedSSL_write(ssl, &operation, sizeof(Operations));
	SSL_writeFileData(ssl, fileData);

	SERVER_STATE serverState;

	chunkedSSL_read(ssl, &serverState, sizeof(SERVER_STATE));
	printf("Here4\n");

	serverState ? printf("Certificate upload successfull\n") : printf("Certificate upload  failed\n");

	closeClient(ssl);
	FILEDATA_destoryFileData(fileData);
}

/*
 *	@name 	uploadFileToServer
 *	@param	
 * 	@info	Contains the protocol code for uploading a file to the server.
 *			Uses the global variable flags as its run time parameters
 *			Shares a lot of code with uploadCertificate but they're subtlety different 
 *	@return 
 */
void uploadFileToServer()
{
	FileData* fileData = readData(flags.fileName);
	
	SSL* ssl = startClient();

	Operations operation = ADD;
	chunkedSSL_write(ssl, &operation, sizeof(Operations));
	SSL_writeFileData(ssl, fileData);

	SERVER_STATE serverState;

	chunkedSSL_read(ssl, &serverState, sizeof(SERVER_STATE));

	serverState ? printf("File transer successfull\n") : printf("Filed transfer failed\n");

	closeClient(ssl);
	FILEDATA_destoryFileData(fileData);
}

/*
 *	@name 	fetchFileFromServer
 *	@param	
 * 	@info	Contains the protocol code for fetching a file to the server.
 *			Uses the global variable flags as its run time parameters.
 *	@return 
 */
void fetchFileFromServer()
{
	SSL* ssl = startClient();

	Operations operation = FETCH;																				//All of these are fixed sizes
	chunkedSSL_write(ssl, &operation, sizeof(Operations));														//Send the operation enum to the server

	chunkedSSL_write(ssl, &flags.requiredTrustCircumference, sizeof(int));										//Then required trust circumference

	chunkedSSL_write(ssl, flags.fileName, EVP_MAX_MD_SIZE / 2);													//Then digest of the file we're looking for

	SERVER_STATE serverState;

	chunkedSSL_read(ssl, &serverState, sizeof(SERVER_STATE));

	switch (serverState)
	{
		case SERVER_STATE_FILE_EXISTS_AND_TRUSTED:																//If the server found the file and a long enough trust chain
		{
			FileData* fileData = SSL_readFileData(ssl);															//Read it into a FileData struct
			printf("Retrieved Filename : %s, Size: %d\n", fileData->fileName, fileData->size);
			writeData(fileData);																				//Write it to the local disk
			FILEDATA_destoryFileData(fileData);
		}
			break;

		case SERVER_STATE_FILE_NOT_TRUSTED:
			fprintf(stderr, "File found but does not have trust level %d or higher.\n", flags.requiredTrustCircumference);
			closeClient(ssl);
			exit(EXIT_FAILURE);
			break;

		case SERVER_STATE_FILE_NOT_FOUND:
			fprintf(stderr, "File found not found with digest %s.\n", flags.fileName);
			closeClient(ssl);
			exit(EXIT_FAILURE);
			break;

		case SERVER_STATE_FILE_TAMPERED:
			fprintf(stderr, "The file has been tampered with\n");
			closeClient(ssl);
			exit(EXIT_FAILURE);
			break;

		default:
			fprintf(stderr, "Server responded with incorrect state %d.\n", serverState);
			closeClient(ssl);
			exit(EXIT_FAILURE);
			break;
	}
	closeClient(ssl);
}

/*
 *	@name 	getListFromServer
 *	@param	
 * 	@info	Contains the protocol code for fetching a list of files and users from the server.
 *			Uses the global variable flags as its run time parameters.
 *	@return 
 */
 /**********************************************************\
 *	N O T     I M P L E M E N T E D    S E R V E R S I D E  *
 \**********************************************************/
void getListFromServer()
{
	SSL* ssl = startClient();

	Operations operation = LIST;
	chunkedSSL_write(ssl, &operation, sizeof(Operations));
	FileData* fileData = SSL_readFileData(ssl);
	printf("%s", fileData->data);
	closeClient(ssl);
}
