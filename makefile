PROJECT	= trustcloud

CC	= gcc -std=c99
CFLAGS	= -Werror -Wall -Wextra -pedantic
LFLAGS  = -l ssl -l crypto

BINDIR	= bin
OBJDIR	= obj
SRCDIR	= src

SRCS	= $(wildcard $(SRCDIR)/*.c)
HEADERS	= $(wildcard $(SRCDIR)/*.h)
OBJS	= $(patsubst %.c, %.o, $(patsubst $(SRCDIR)/%, $(OBJDIR)/%, $(SRCS)))

$(PROJECT) : $(OBJS)
	$(CC) $(CFLAGS) -o $(BINDIR)/$(PROJECT) $(OBJS) $(LFLAGS)

$(OBJS) : $(OBJDIR)/%.o : $(SRCDIR)/%.c $(HEADERS)
	$(CC) $(CFLAGS) -o $@ -c $< 

info:
	@echo	$(SRCS)
	@echo	$(OBJS)
	@echo	$(HEADERS)

clean:
	rm -f $(BINDIR)/$(PROJECT) $(OBJS)
